# pcp-sysbench-scripts

Scripts to run sysbench cpu and memory tests with and without pmcd and pmlogger enabled.


## Requirements

Performance Co-Pilot - for performance metrics

Sysbench - to generate workload

Rt-tests - cyclictest is used to get latency - git://git.kernel.org/pub/scm/utils/rt-tests/rt-tests.git 

## Installation

Use git to clone this repository into your computer.

git clone https://gitlab.com/redhat/edge/tests/perfscale/pcp-sysbench-scripts

## Usage

PCP_sysbench.sh 

parameters:

-p    To run sysbench with PCP enable/disabled yes/no value

-t    Run sysbench cpu or memory test cpu/mem

-l    Number of loops 

-f    Pmlogger log file - it will also be used as the directory name for each test in the format <log file name>_<loop #> 

-r    PCP logging interval, by default 1s


Example: PCP_sysbench.sh -p y -t mem -f sysbench_pcp_mem -l 10 -r 250msec

## Process test results

Get cyclictest latency data

get-cyclictest-latency

parameters:

-d   test directory, do not include the '<_loop#> part of the directory name

-f   file name of the csv file to be created

Example: get-cyclictest-latency -d sysbench_cpu_pcp -f sysbench_cpu_latency

Get metric data from pmlogger archive files

get-pcp-metrics

parameters:

-d   test directory, do not include the '<_loop#> part of the directory name

-f   file name of the csv file to be created

-m   PCP metric

Example: get-pcp-metrics -d sysbench_pcp_mem_r500msec -f mem_used_r500msec -m mem.util.used

Create sysbench tests csv files

make-gsheet-mem

make-gsheet-pcp

Parameters:

-d   test directory, do not include the '<_loop#> part of the directory name

-p    If test ran with PCP enable/disabled yes/no value

-t    Sysbench test cpu/mem value

-c    Number of cpus used in the test 

-f   file name of the csv file to be created

-r    PCP logging interval

