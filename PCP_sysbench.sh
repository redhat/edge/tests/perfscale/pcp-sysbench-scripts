#!/bin/bash

# Script to test PCP with sysbench

export DURATION=${DURATION:-300}
export LAT_THRES=${LAT_THRES:-40}
export SMI_THRES=${SMI_THRES:-40}
export LAT_LOOP=${LAT_LOOP:-1}
export loops=${loops:-1}


rate=1
board="R4"
log_file="sysbench"
test_dir=$(pwd)
c_version=`cyclictest -h |head -1 |awk '{print $3}'`
if [[ $(bc <<< "$c_version < 2.60") -eq 1 ]]; then
    echo "Cyclictest version: $c_version, we'll use -n flag" 
    n_flag=' -n'
else
    echo "Cyclictest version: $c_version"
    n_flag=''
fi

# Get params
# -p    To run sysbench with PCP enable/disabled yes/no value
# -t    Run sysbench cpu or memory test cpu/mem
# -l    Number of loops 
# -f    Pmlogger log file 
# -d    Test duration in seconds
# -r    Interval rate

# PCP
start_pcp()
{
    # Start PCP
    systemctl enable pmcd
    systemctl start pmcd
    # Start pmlogger
    systemctl enable pmlogger
    systemctl start pmlogger 
    pml_file=${test_dir}/${log_file}_$(date -I)_rate_${rate}
    echo "pml_file is $pml_file "
    pmlogger -c /var/lib/pcp/config/pmlogger/config.default -t $rate  $pml_file &
}

stop_pcp()
{
    # Stop pmlogger
    systemctl stop pmlogger
    pkill pmlogger
    # Stop PCP
    systemctl stop pmcd
}

run_sysbench()
{
# Sysbench

if [[ $sys_test == "cpu" ]]; then
    # Start cpu test
    echo "Starting sysbench cpu test ..."
    runtest_cpu
elif [[ $sys_test =~ "mem" ]]; then
    # Start memory test
    echo "Starting sysbench memory test ..."
    runtest_mem
else
    echo "Please enter cpu or mem"
fi

}

function runtest_cpu()
{
    taskset -c $cpu_list sysbench cpu run --threads=$isolate_num --time=$DURATION --percentile=99 --cpu-max-prime=200000 |tee sysbench_cpu.out &

    echo "use cyclictest to measure system latency"
    cmdline="taskset -c $cpu_list cyclictest -m -q -p95 -D $DURATION -h40 -i 100 -t $isolate_num -a $cpu_list $n_flag "
    echo "$cmdline" 
   for i in $(seq 1 $LAT_LOOP); do
        $cmdline | tee cyclic_cpu.out
        echo "$i time:" | tee -a lat_cyclic_cpu.out
        egrep '(Min|Avg|Max) Latencies' cyclic_cpu.out | tee -a lat_cyclic_cpu.out
    done

    max_val=$(grep "Max Latencies" lat_cyclic_cpu.out | awk -F": " '{print $2}' | tr ' ' '\n' | awk '$0>x {x=$0}; END{print x}'
)
    max_val=$((10#${max_val}))
    echo "Maximum latency: $max_val" | tee -a max_cyclic_cpu.out

    echo "Max Latency with $LAT_LOOP times, echo for $DURATION: $max_val"
    if [ $max_val -gt $LAT_THRES ]; then
        echo "$max_val is higher than $LAT_THRES"
    else
        echo "$max_val is lower than $LAT_THRES"
    fi
}

function runtest_mem()
{

## READS ##
taskset -c $cpu_list sysbench memory run --threads=$isolate_num --memory-block-size=1K --memory-access-mode=rnd --percentile=99 --memory-oper=read --memory-total-size=16024G --time=$DURATION |tee sysbench_mem.out &

   echo "use cyclictest to measure system latency"
   cmdline="taskset -c $cpu_list cyclictest -m -q -p95 -D $DURATION -h40 -i 100 -t $isolate_num -a $cpu_list $n_flag "
   echo "$cmdline" 
   for i in $(seq 1 $LAT_LOOP); do
        $cmdline | tee cyclic_mem.out
        echo "$i time:" | tee -a lat_cyclic_mem.out
        egrep '(Min|Avg|Max) Latencies' cyclic_mem.out | tee -a lat_cyclic_mem.out
    done

    max_val=$(grep "Max Latencies" lat_cyclic_mem.out | awk -F": " '{print $2}' | tr ' ' '\n' | awk '$0>x {x=$0}; END{print x}'
)
    max_val=$((10#${max_val}))
    echo "Maximum latency: $max_val" | tee -a max_cyclic_mem.out

    echo "Max Latency with $LAT_LOOP times, echo for $DURATION: $max_val"
    if [ $max_val -gt $LAT_THRES ]; then
        echo "$max_val is higher than $LAT_THRES"

    else
        echo "$max_val is lower than $LAT_THRES"

    fi
}

#--- START ---#

# Get arguments
while getopts p:t:l:f:d:r:b: flag    
do
    case "${flag}" in
        p) pcp=${OPTARG};;
        t) sys_test=${OPTARG};;
        l) loops=${OPTARG};;
	    f) log_file=${OPTARG};;
	    d) DURATION=${OPTARG};;
	    r) rate=${OPTARG};;
        b) board=${OPTARG};;
    esac
done
echo "PCP: $pcp";
echo "Test: $sys_test";
echo "Loops: $loops";
echo "Log file: $log_file";
echo "Duration in secs: $DURATION"
echo "Interval rate: $rate";
echo "Board: $board";

nrcpus=8
if [[ $board == "QD3" ]]; then
# Run on upper 4 CPUs    
    if [ $nrcpus -lt 4 ]; then
        echo "recommend running measure process on >= 4 CPUs machine"
        exit 0
    else
        c_low=$(( nrcpus / 2 ))
        c_high=$(( nrcpus - 1 ))
        cpu_list=$c_low"-"$c_high
        isolate_num=$(( c_high - c_low + 1 ))
    fi
else
    cpu_list="0-7"
    isolate_num=8
fi

# Run tests $loop times

x=1
cur_dir=$(pwd)
while [ $x -le $loops ]
do
    # Create new test directory
    echo -e "\n ***** Starting test number $x *****\n"
    cd $cur_dir
    echo "cur_dir: $cur_dir"
    if [[ $pcp == "n" ]]; then
        test_dir=${cur_dir}/${log_file}_no_pcp_$x
    else
        test_dir=${cur_dir}/${log_file}_r${rate}_$x
    fi
    echo "Creating dir: $test_dir"
    mkdir $test_dir
    cd $test_dir
    echo "Current directory: $(pwd)"
    # PCP
    if [[ $pcp == "n" ]]; then
        # Stop PCP and pmlogger
        echo "Stop PCP and pmlogger"
        stop_pcp
## Get mem/cpu info when PCP is not running
top -d 1 -b|grep "load average" -A 15 >> top.log &
    else
        # Start PCP and pmlogger
        echo "Start PCP and pmlogger"
        start_pcp
        sleep 5
    fi
    # Sysbench
    run_sysbench
    sleep 5
    echo "Stopping PCP"

pkill top
    stop_pcp
    x=$((x + 1))
    sleep 60
done
exit 0

